####
#### Temporary layer to prepare installation
####
FROM postgres:alpine AS build

MAINTAINER EOLE <eole@ac-dijon.fr>

RUN apk add --no-cache curl

ARG CONTAINERPILOT_VERSION=3.4.3
ARG CONTAINERPILOT_CHECKSUM=e8258ed166bcb3de3e06638936dcc2cae32c7c58

RUN curl -Lso /tmp/containerpilot.tar.gz \
         "https://github.com/joyent/containerpilot/releases/download/${CONTAINERPILOT_VERSION}/containerpilot-${CONTAINERPILOT_VERSION}.tar.gz" \
    && echo "${CONTAINERPILOT_CHECKSUM}  /tmp/containerpilot.tar.gz" | sha1sum -c \
    && tar zxf /tmp/containerpilot.tar.gz -C /tmp


####
#### Target layer
####
FROM postgres:alpine

# Install tools from build layer
COPY --from=build /tmp/containerpilot /usr/local/bin
RUN chmod +x /usr/local/bin/*

# add ContainerPilot configuration file
COPY containerpilot.json5 /etc/containerpilot.json5

COPY docker-entrypoint-initdb.d /docker-entrypoint-initdb.d
RUN chmod +x /docker-entrypoint-initdb.d/*.sh
COPY docker-health.d/ /docker-health.d
RUN chmod +x /docker-health.d/*

CMD ["/usr/local/bin/containerpilot", "-config", "/etc/containerpilot.json5"]
